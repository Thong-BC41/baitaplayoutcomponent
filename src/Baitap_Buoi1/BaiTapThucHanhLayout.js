import React, { Component } from "react";
import Header from "./Header";
import Footer from "./Footer";
import Banner from "./Banner";
import Item from "./Item";

export default class BaiTapThucHanhLayout extends Component {
  render() {
    return (
      <div>
        <Header />
        <Banner />
        <section class="pt-4">
          <div class="container px-lg-5">
            <div class="row gx-lg-5">
              <div class="col-lg-6 col-xl-4 mb-5">
                <Item />
              </div>
              <div class="col-lg-6 col-xl-4 mb-5">
                <Item />
              </div>
              <div class="col-lg-6 col-xl-4 mb-5">
                <Item />
              </div>
            </div>
            <div class="row gx-lg-5">
              <div class="col-lg-6 col-xl-4 mb-5">
                <Item />
              </div>
              <div class="col-lg-6 col-xl-4 mb-5">
                <Item />
              </div>
              <div class="col-lg-6 col-xl-4 mb-5">
                <Item />
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}
