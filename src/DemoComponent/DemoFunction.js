import React from "react";

export default function DemoFunction() {
  return (
    <div>
      <h1>Demo Function</h1>
      <p>This is a demo function</p>
      <button class="btn btn-primary">TEST</button>
    </div>
  );
}
