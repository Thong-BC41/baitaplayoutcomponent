import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import Ex_Layout from "./Ex_Layout/Ex_Layout";
import DataBinding from "./DataBinding/DataBinding";
import BaiTapThucHanhLayout from "./Baitap_Buoi1/BaiTapThucHanhLayout";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoClass />
      <DemoFunction></DemoFunction>
      <DemoFunction></DemoFunction> */}
      {/* <Ex_Layout /> */}
      {/* <DataBinding /> */}
      <BaiTapThucHanhLayout />
    </div>
  );
}

export default App;
